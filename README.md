ics-ans-role-ioclogserver
============================

Ansible role to install iocLogServer on CentOS 7.3.

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

```
# EPICS variables
epicsioclogserver_epics_ioc_log_inet: 10.4.6.64
epicsioclogserver_epics_ioc_log_port: 7004
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-ioclogserver
```

License
-------

BSD 2-clause

The following templates come from the official Apache Tomcat installation and are included under the [Apache 2.0 license](http://www.apache.org/licenses/LICENSE-2.0.txt):

- templates/context.xml.j2
- templates/server.xml.j2
- templates/tomcat-users.xml.j2
- templates/web.xml.j2

A local copy of the licenses can be found in the files LICENSE and Apache_2.0_License.txt.
